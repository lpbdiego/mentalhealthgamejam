using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class groundCheck : MonoBehaviour
{
    public bool isGrounded;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //Grounded stuff
    private void OnCollisionEnter2D(Collision2D theCollision)
    {
      
        if (theCollision.gameObject.CompareTag("ground"))
        {
            isGrounded = true;
        }
    }


    //consider when character is jumping .. it will exit collision.
    void OnCollisionExit2D(Collision2D theCollision)
    {
       
        if (theCollision.gameObject.CompareTag("ground"))
        {
            isGrounded = false;
        }
    }
}
