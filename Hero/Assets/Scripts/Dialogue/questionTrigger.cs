using UnityEngine;
using TMPro;
/*
Add this script to an object with Box Collider2D
with the layer set as Enemy and with the trigger
of the BoxColider checked
 */
public class questionTrigger : MonoBehaviour
{
public static bool questionIsOn = false;
 [Header ("Drag questionMenu Prefab here:")]
    [SerializeField] public GameObject questionMenu;
    [SerializeField] private string typeOfQuestion;

    [SerializeField] private string NextQuestion1;
    [SerializeField] private string NextQuestion2;
    [SerializeField] public GameObject questionText;
    [SerializeField] public GameObject answerText;
    [SerializeField] public GameObject triggerQuestion;
    save_player_psyche savePlayerPsyche;
    bool questionDone;
    bool pleaseanswer;

    public GameObject Sword1;
    public GameObject Sword2;
    public GameObject Sword3;


    void Awake() {
        savePlayerPsyche = new save_player_psyche();
        questionDone = false;
        pleaseanswer = false;
    }

    private void Update()
    {
        if(triggerQuestion.activeSelf && !questionDone)
        {
            StartQuestion();
        }
    }
    public void StartQuestion()
    {
        if (!questionDone)
        {
            questionMenu.SetActive(true);
            Time.timeScale = 0f;
            questionIsOn = true;
            
             switch(typeOfQuestion)
            {
                case "name":
                    questionText.GetComponent<TMP_Text>().text = "  Whats your name?";
                    break;
                case "problem":
                    questionText.GetComponent<TMP_Text>().text = "What kind of problem you have?";
                    break;
                case "motivation1":
                   questionText.GetComponent<TMP_Text>().text = "Who is one person that motivates you? That person, the one you just thought about";
                    break;
                case "motivation2":
                    questionText.GetComponent<TMP_Text>().text = "What is one thing that makes you happy? It could be anything that makes you smile. ";
                    break;
                case "motivation3":
                    questionText.GetComponent<TMP_Text>().text = "What is an important goal for you? Simply something you want to achieve.";
                    break;
                default:
                    
                    break;
            }
        }
        
    }
     public void Resume()
    {
        if (!questionDone && answerText.GetComponent<TMP_InputField>().text !="")
        {
            savePlayerPsyche.SavePsyche(answerText.GetComponent<TMP_InputField>().text, typeOfQuestion);
            if ( NextQuestion1 == "" || NextQuestion1 == "none")
            {
                questionMenu.SetActive(false);
                Time.timeScale = 1f;
                questionIsOn = false;
                questionDone = true;
            }
            else 
            {
                typeOfQuestion =NextQuestion1;
                if (NextQuestion2 == "" || NextQuestion2 == "none")
                {
                    NextQuestion1 = "none";
                }
                else 
                {
                    NextQuestion1 = NextQuestion2;
                    NextQuestion2 = "none";
                }
                answerText.GetComponent<TMP_InputField>().text= "";
                StartQuestion();
               
            }
            
        }
        else if(!questionDone && !pleaseanswer) {
            questionText.GetComponent<TMP_Text>().text= questionText.GetComponent<TMP_Text>().text + " please answer";
            pleaseanswer = true;
        }

    }
}
