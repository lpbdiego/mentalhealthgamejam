using UnityEngine;
using UnityEngine.SceneManagement;
using FMODUnity;
using FMOD.Studio;

public class pauseMenu : MonoBehaviour
{
  
    private FMOD.Studio.Bus dialogueBus;

    void Awake()
    {
        dialogueBus = FMODUnity.RuntimeManager.GetBus("bus:/Dialogue");
    }


    //IN ORDER FOR THE PAUSEMENU PREFAB TO WORK, YOU NEED TO ADD THIS SCRIPT TO THE CANVAS OF THE SCENE YOU ARE ON AND DRAG PAUSEMENU INTO IT
    public static bool GameIsPaused = false;
    [Header ("Drag PauseMenu Prefab here:")]
    public GameObject pauseMenuUI;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }
    public void Resume()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UISounds/ButtonPress");
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        dialogueBus.setPaused(GameIsPaused);
    }

    public void Pause()
    {
        if (!(Time.timeScale == 0f))
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/UISounds/ButtonPressQuit");
            pauseMenuUI.SetActive(true);
            Time.timeScale = 0f;
            GameIsPaused = true;
        dialogueBus.setPaused(GameIsPaused);
        }
    }

    public void QuitGame()
    {
        SceneManager.LoadScene("0MainMenu");
        Time.timeScale = 1f;
        GameIsPaused = false;

    }
}
