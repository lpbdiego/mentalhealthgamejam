using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dialogTrigger : MonoBehaviour
{
   [SerializeField] public GameObject Dialog;
    bool DialogDone;

    void Awake() {
        DialogDone= false;
    }
     private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag=="Player")
        {
            StartDialog();
         }
    }
    public void StartDialog()
    {
        if (!DialogDone)
        {
            Dialog.SetActive(true);
            DialogDone=true;
        }
        
    }
}
