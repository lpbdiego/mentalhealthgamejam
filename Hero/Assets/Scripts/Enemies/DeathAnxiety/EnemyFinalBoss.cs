using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class EnemyFinalBoss : MonoBehaviour
{
    [Header("Enemy Stats")]
    [SerializeField] public float speed;
    [SerializeField] public float retreatSpeed;
    [SerializeField] public float range;
    [SerializeField] public float attackRange;
    [SerializeField] public int maxHealth;
    [SerializeField] public int health;
    [SerializeField] public int damageCooldown;

    private float coolDownTimer;
    private float lastTimeHurt;

    [SerializeField] public float stoppingDistance;
    [SerializeField] public float retreatDistance;

    [Header ("Enemy Functionality")]
    [SerializeField] private bool chase = false;
    [SerializeField] private GameObject player;
    [SerializeField] private Vector2 startPos;
    [SerializeField] public float deathDelayTime;
    [SerializeField] public bool inAttackRange;
    [SerializeField] public GameObject gigaExplosion;
    [SerializeField] public GameObject gigaUltraExplosion;
    [SerializeField] public float scaleChange = 1.1f;
    [SerializeField] public GameObject finalPortal;
    [SerializeField] public GameObject wings;
    [SerializeField] private healthBarScript healthBar;

    [SerializeField] GameObject musicManager;
    private FMODUnity.StudioEventEmitter emitter;
    //[SerializeField] private string BossHealth;
    //[SerializeField] private float parameterValue;

    [SerializeField] GameObject enemy;

    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
        player = GameObject.FindGameObjectWithTag("Player");
        startPos = gameObject.transform.position;
        healthBar.UpdateHealthBar(maxHealth, health);
        musicManager = GameObject.FindGameObjectWithTag("Music");
        emitter = musicManager.GetComponent<FMODUnity.StudioEventEmitter>();
        emitter.SetParameter("BossHealth", health);
    }

    // Update is called once per frame
    void Update()
    {


        if (player == null)
        {
            return;
        }
        if (chase == true)
        {
            if (!inAttackRange)
            {
                Chase();
            }
            FlipTowardsPlayer();
        }


        if (Vector2.Distance(player.transform.position, transform.position) <= range && (Vector2.Distance(player.transform.position, transform.position) >= stoppingDistance))
        {
            inAttackRange = false;
            chase = true;
        }
        else if ((Vector2.Distance(player.transform.position, transform.position) <= range) && (Vector2.Distance(player.transform.position, transform.position) <= stoppingDistance) && (Vector2.Distance(player.transform.position, transform.position) >= retreatDistance)) {
            inAttackRange = true;
            transform.position = this.transform.position;
        }

        else if ((Vector2.Distance(player.transform.position, transform.position) <= retreatDistance))
        {
            Retreat();
        }
        else
        {
            chase = false;
        }

        if (health <= 0)
        {
            //ULTRA EXPLOSION (1 EXPLOSION PER FRAME) REMOVE IF DANGEROUS LOL
            Instantiate(gigaExplosion, transform.position, transform.rotation);
            healthBar.UpdateHealthBar(maxHealth, health);
            StartCoroutine("Die");
            
           // animator.SetTrigger("death");
        }
    }

    private void Chase()
    {
        transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);

        //Attack when Close Enough to Player
        if (Vector2.Distance(transform.position, player.transform.position) <= attackRange)
        {
            //attack
        }
    }

    private void Retreat()
    {
        transform.position = Vector2.MoveTowards(transform.position, player.transform.position, -retreatSpeed * Time.deltaTime);

    }

    public void FlipTowardsPlayer()
    {
        if (transform.position.x > player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        } else
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);     
        }
    }


    private void ReturnToStartingPoint()
    {
        transform.position = Vector2.MoveTowards(transform.position, startPos, speed * Time.deltaTime);
        inAttackRange = false;
    }

    private IEnumerator Die()
    {
        
        yield return new WaitForSeconds(deathDelayTime);
        FMODUnity.RuntimeManager.PlayOneShotAttached("event:/Enemies/Boss EnemyDie", enemy);
        Instantiate(gigaUltraExplosion, transform.position, transform.rotation);
        Instantiate(finalPortal, transform.position, transform.rotation);
        Destroy(gameObject);
        
    }

    //Take Damage From Sword or Slash

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Slash"))
        {
            Hurt();
            //FMODUnity.RuntimeManager.PlayOneShotAttached("event:/Enemies/DeathEnemyDie", enemy);
            healthBar.UpdateHealthBar(maxHealth, health);
        }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("boardingSword") || other.gameObject.CompareTag("DeflectedProjectile"))
        {
            Hurt();
            // FMODUnity.RuntimeManager.PlayOneShotAttached("event:/Enemies/DeathEnemyDie", enemy);
            healthBar.UpdateHealthBar(maxHealth, health);
        }
    }

    private void Hurt()
    {
        //Attack Cooldown Logic
        if (Time.time - lastTimeHurt < damageCooldown)
        {
            return;
        }
        lastTimeHurt = Time.time;
        healthBar.UpdateHealthBar(maxHealth, health);
        Instantiate(gigaExplosion, transform.position, transform.rotation);
        health--;
        emitter.SetParameter("BossHealth", health);
        //Grow Bigger
        Vector2 objectScale = transform.localScale;
        transform.localScale = new Vector2(objectScale.x * scaleChange, objectScale.y * scaleChange);
        wings.SetActive(true);
    }
}
