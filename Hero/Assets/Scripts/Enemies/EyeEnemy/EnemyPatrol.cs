using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    [Header ("Patrol Points")]
    [SerializeField] private Transform leftEdge;
    [SerializeField] private Transform rightEdge;

    [Header ("Enemy")]
    [SerializeField] public Transform enemy;

    [Header ("Movement parameters")]
    [SerializeField] private float speed;
    private Vector2 initScale;
    private void Awake()
    {
        initScale = enemy.localScale;
    }
    private void Update()
    {
        MoveInDirection(1);
    }

    private void MoveInDirection(int _direction)
    {
     //Make enemy face direction
        enemy.localScale = new Vector2(Mathf.Abs(initScale.x) * _direction,
         initScale.y);

        //Move in direction
        enemy.position = new Vector2( enemy.position.x + Time.deltaTime * _direction * speed,
             enemy.position.y);
    }
}
