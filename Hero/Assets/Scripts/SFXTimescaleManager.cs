using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using FMODUnity;
using FMOD.Studio;

public class SFXTimescaleManager : MonoBehaviour
{
    
    [SerializeField] EventReference aGroupSnapshot;

    private FMOD.Studio.EventInstance snapshotInstance;

    private void Awake()
    {
        snapshotInstance = FMODUnity.RuntimeManager.CreateInstance(aGroupSnapshot);
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 0f)
        {
            StartSnapshot();
        }

        else
        {
            StopSnapshot();
        }
    }


    private void StartSnapshot()
    {
        snapshotInstance.start();
    }

    private void StopSnapshot()
    {
        snapshotInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT); 
    }

}
