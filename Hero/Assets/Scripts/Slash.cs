using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slash : MonoBehaviour
{
    public float SecondsToWait;
    void Start()
    {
        StartCoroutine(disappearExplosion());
    }

    IEnumerator disappearExplosion()
    {
        yield return new WaitForSeconds(SecondsToWait);
        Destroy(gameObject);
    }

    //Dealing Damage
    /*
    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        Enemy enemy = hitInfo.GetComponent<Enemy>();

        if (enemy != null)
        {
            enemy.TakeDamage(1);
        }
    */
    }
