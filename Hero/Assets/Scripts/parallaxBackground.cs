using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class parallaxBackground : MonoBehaviour
{
    public Camera cam;
    public Transform subject;
    Vector2 startPosition;
    float startz;



    Vector2 Travel => (Vector2)cam.transform.position - startPosition;

    float distanceFromSubject => transform.position.z - subject.position.z;
    float clippingPlane => (cam.transform.position.z) + (distanceFromSubject > 0 ? cam.farClipPlane : cam.nearClipPlane);

    float parallaxFactor => Mathf.Abs(distanceFromSubject) / clippingPlane;

    private void Awake()
    {
       // cam = Camera.main;
    }
    public void Start()
    {

        startPosition = subject.position;
        startz = transform.position.z;
    }


    public void Update()
    {
        Vector2 newPos = startPosition + Travel * parallaxFactor;
        transform.position = new Vector3(newPos.x, newPos.y, startz);

    }



}
