using UnityEngine;
using UnityEngine.UI;
using FMODUnity;

public class ABVolumeSliderOption1 : MonoBehaviour
{
    [SerializeField]
    private Slider slider = null;

    [SerializeField]
    private string VCAPath = "";

    private FMOD.Studio.VCA VCA;
    

    // Start is called before the first frame update
    private void Start()
    {
        if (VCAPath != "")    
        {
            VCA = RuntimeManager.GetVCA(VCAPath);
        }

        VCA.getVolume(out float volume);
        slider.value = volume * slider.maxValue;

        UpdateSliderOutput();
    }

    public void UpdateSliderOutput()
    {
        if (slider != null)
        {
            VCA.setVolume(slider.value / slider.maxValue);
        }
    }
}
