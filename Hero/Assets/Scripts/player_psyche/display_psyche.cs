using UnityEngine;

public class display_psyche : MonoBehaviour
{
    [SerializeField] public GameObject triggerActivity;
    bool started;
    void Awake() 
    {
        started = false;
    }
    void Update() 
    {
        if (triggerActivity.activeSelf && !started){
            Debug.Log("It triggers");
            started = true;
        }
    }
}
