using System.Collections;
using TMPro;
using UnityEngine;

public class ShowName : MonoBehaviour
{
    [SerializeField] public GameObject NameDisplay;
    // Start is called before the first frame update
    void Start()
    {

         NameDisplay.GetComponent<TMP_Text>().text = "Thank you for playing " + players_psyche.username + "!";
        
    }

    
}
