using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class explosionScript : MonoBehaviour
{
    public float SecondsToWait;
    void Start()
    {
        StartCoroutine(disappearExplosion());
    }

    IEnumerator disappearExplosion()
    {
        yield return new WaitForSeconds(SecondsToWait);
        Destroy(gameObject);
    }
}
