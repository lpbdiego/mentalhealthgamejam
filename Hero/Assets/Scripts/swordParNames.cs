using System.Collections;
using UnityEngine;
using TMPro;
public class swordParNames : MonoBehaviour
{

[SerializeField] public GameObject part1;
[SerializeField] public GameObject part2;
[SerializeField] public GameObject part3;

[SerializeField] public GameObject playerAnswer;
bool notfilled;
int counter;
    // Start is called before the first frame update
    void Start()
    {
        notfilled = true;
        counter =1;
        part1.GetComponent<TMP_Text>().text = players_psyche.usermotivation1;
        part2.GetComponent<TMP_Text>().text = players_psyche.usermotivation2;
        part3.GetComponent<TMP_Text>().text = players_psyche.usermotivation3;
    }

    public void updateSword() 
    {   
        if (notfilled )
        {
            if(counter == 1)
            {
                Debug.Log("First: "+playerAnswer.GetComponent<TMP_InputField>().text);
                part1.GetComponent<TMP_Text>().text = playerAnswer.GetComponent<TMP_InputField>().text;
                counter++;
            }
            else if(counter == 2)
            {
                Debug.Log("Sceond: "+playerAnswer.GetComponent<TMP_InputField>().text);
                part2.GetComponent<TMP_Text>().text = playerAnswer.GetComponent<TMP_InputField>().text;
                counter++;
            }
            else if(counter == 3)
            {
                Debug.Log("Third: "+playerAnswer.GetComponent<TMP_InputField>().text);
                part3.GetComponent<TMP_Text>().text = playerAnswer.GetComponent<TMP_InputField>().text;
                counter++;
            }
           
        }
    }


}
