using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boardingSwordFollowing : MonoBehaviour
{

    public GameObject Player;
    private GameObject followTargetObject;
    public float followSpeed;
    public Transform followTarget;
    public playerHero PlayerScript;
    public int rotationRate;
    public bool rightOnYourBack;

    private void Start()
    {
        //Rotate the Sword into vertical Position
        transform.rotation = Quaternion.Euler(0, 0, 135);
        rightOnYourBack = true;
        //Reference the Player's GameObject
        Player = GameObject.FindGameObjectWithTag("Player");
        //Reference the Player's Script 
        PlayerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<playerHero>();
        //Assign this Sword as the current Sword
        PlayerScript.CurrentSword = gameObject;
        

        //IMPORTANT: followTargetObject must be the second Child inside the PLayer Prefab!
        GameObject FollowTargetObject = Player.transform.GetChild(1).gameObject;
        followTarget = FollowTargetObject.transform;
    }
    private void Update()
    {
     

            transform.position = Vector3.Lerp(transform.position, followTarget.position, followSpeed * Time.deltaTime);
        
    }

    private void FixedUpdate()
    {
        if (!rightOnYourBack)
        {
            transform.Rotate(0, 0, rotationRate * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("RightOnYourBack"))
        {
            rightOnYourBack = true;
            //transform.rotation = Quaternion.Euler(0, 0, 90);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("RightOnYourBack"))
        {
            rightOnYourBack = true;
        }
    }
}
