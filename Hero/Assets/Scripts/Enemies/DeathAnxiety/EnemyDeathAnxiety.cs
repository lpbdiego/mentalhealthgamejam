using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class EnemyDeathAnxiety : MonoBehaviour
{
    [Header("Enemy Stats")]
    [SerializeField] public float speed;
    [SerializeField] public float range;
    [SerializeField] public float attackRange;
    [SerializeField] public int health;
    [SerializeField] public float stoppingDistance;
    [SerializeField] public float retreatDistance;
    [SerializeField] public float timeBtwShots;

    [Header ("Enemy Functionality")]
    [SerializeField] private bool chase = false;
    [SerializeField] private GameObject player;
    [SerializeField] private Vector2 startPos;
    [SerializeField] public float deathDelayTime;
    [SerializeField] public float AttackDelayTime;
    [SerializeField] public Animator animator;
    [SerializeField] public bool inAttackRange;
    public float startTimeBtwShots;

    public GameObject projectile;
    public GameObject shootPosition;

   

    [SerializeField] GameObject enemy;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        startPos = gameObject.transform.position;
        timeBtwShots = startTimeBtwShots;
    }

    // Update is called once per frame
    void Update()
    {
        //Attack Cooldown
        if(timeBtwShots <= 0 && chase && health > 0)
        {
            animator.SetTrigger("attack");
            StartCoroutine("Attack");
            timeBtwShots = startTimeBtwShots;

        } else
        {
            timeBtwShots -= Time.deltaTime;
        }


        if (player == null)
        {
            return;
        }
        if (chase == true)
        {
            if (!inAttackRange)
            {
                Chase();
            }
            FlipTowardsPlayer();
        } else
        {
            ReturnToStartingPoint();
        }


        if (Vector2.Distance(player.transform.position, transform.position) <= range && (Vector2.Distance(player.transform.position, transform.position) >= stoppingDistance))
        {
            inAttackRange = false;
            chase = true;
        }
        else if ((Vector2.Distance(player.transform.position, transform.position) <= range) && (Vector2.Distance(player.transform.position, transform.position) <= stoppingDistance) && (Vector2.Distance(player.transform.position, transform.position) >= retreatDistance)) {
            inAttackRange = true;
            transform.position = this.transform.position;
        }

        else if ((Vector2.Distance(player.transform.position, transform.position) <= retreatDistance))
        {
            Retreat();
        }
        else
        {
            chase = false;
        }

        if (health <= 0)
        {
            StartCoroutine("Die");
            
            animator.SetTrigger("death");
        }
    }

    private void Chase()
    {
        transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);

        //Attack when Close Enough to Player
        if (Vector2.Distance(transform.position, player.transform.position) <= attackRange)
        {
            //attack
        }
    }

    private void Retreat()
    {
        transform.position = Vector2.MoveTowards(transform.position, player.transform.position, -speed * Time.deltaTime);

    }

    public void FlipTowardsPlayer()
    {
        if (transform.position.x > player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        } else
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);     
        }
    }


    private void ReturnToStartingPoint()
    {
        transform.position = Vector2.MoveTowards(transform.position, startPos, speed * Time.deltaTime);
        inAttackRange = false;
    }

    private IEnumerator Die()
    {
        
        yield return new WaitForSeconds(deathDelayTime);
        Destroy(gameObject);
        
    }

    //Take Damage From Sword or Slash

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Slash"))
        {
            health--;
            FMODUnity.RuntimeManager.PlayOneShotAttached("event:/Enemies/DeathEnemyDie", enemy);
        }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("boardingSword") || other.gameObject.CompareTag("DeflectedProjectile"))
        {
            health--;
            FMODUnity.RuntimeManager.PlayOneShotAttached("event:/Enemies/DeathEnemyDie", enemy);

        }
    }

    private IEnumerator Attack()
    {
        if (health > 0) {
            FMODUnity.RuntimeManager.PlayOneShotAttached("event:/Bullets/BulletChargeUp", enemy);
            yield return new WaitForSeconds(AttackDelayTime);
            Instantiate(projectile, shootPosition.transform.position, Quaternion.identity);
        }
    }
}
