using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using FMOD.Studio;

public class CreepyAmbientLevel0 : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("AmbientDarknessLevel", 0);
        
    }

    // void Start()
    // {
    //     FMODUnity.RuntimeManager.StudioSystem.setParameterByName("EnterAction", 0);
    // }
}
