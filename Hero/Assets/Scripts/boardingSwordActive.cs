using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boardingSwordActive : MonoBehaviour
{

    public GameObject Player;
    public GameObject followTargetObject;
    public GameObject Wheels;
   

    public float followSpeed;
    private Transform followTarget;

    public GameObject FollowTargetObject;
    private bool SwordisFollowing;
    public playerHero PlayerScript;

    private void Start()
    {
        //Reference the Player's GameObject
        Player = GameObject.FindGameObjectWithTag("Player");
        //Reference the Player's Script 
        PlayerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<playerHero>();
        //Assign this Sword as the current Sword
        PlayerScript.CurrentSword = this.gameObject;
    }
    /* COMMENTED EVERYTHING BECAUSE WE PROBABLY DONT NEED THIS BEHAVIOR IF WE USE 2 SWORDS TO CREATE THE EFFECT
    private void Start()
    {
        SwordisFollowing = false;
        Player = GameObject.FindGameObjectWithTag("Player");
        followTarget = FollowTargetObject.transform;
    }
    private void Update()
    {
        //Call Sword Back
        if (Input.GetKeyDown("x") && !SwordisFollowing)
        {
            SwordisFollowing = true;
            GetComponent<CapsuleCollider2D>().enabled = false;                        
            Wheels.gameObject.SetActive(false);
            transform.Rotate(0, 0, 90);

            //This keeps the sword from shaking once you pull it to you when pressing x
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX |
                                                      RigidbodyConstraints2D.FreezePositionY |
                                                      RigidbodyConstraints2D.FreezeRotation;
            Debug.Log("Sword is following? " + SwordisFollowing);

        }
        else if (Input.GetKeyDown("x") && SwordisFollowing)
        {
            SwordisFollowing = false;       
            GetComponent<CapsuleCollider2D>().enabled = true;
            Wheels.gameObject.SetActive(true);
            transform.Rotate(0, 0, -90);

            //This returns the constraints back to normal after releasing the sword
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
            Debug.Log("Sword is following? " + SwordisFollowing);
        }

        if (SwordisFollowing)
        {
            transform.position = Vector3.Lerp(transform.position, followTarget.position, followSpeed * Time.deltaTime);

        }
    }
    */
}
