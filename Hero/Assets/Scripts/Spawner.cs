using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject spawnee;
    public bool stopSpawning = false;

    [HideInInspector]
    public float spawnTime;

    public float spawnDelay;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnObject", spawnTime, spawnDelay);
    }

    public void SpawnObject()
    {
        if (!stopSpawning)
        {
            Instantiate(spawnee, transform.position, transform.rotation);

            if (stopSpawning)
            {
                CancelInvoke("SpawnObject");
            }
        }
    }

}
