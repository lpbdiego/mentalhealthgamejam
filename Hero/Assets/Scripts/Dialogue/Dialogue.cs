using System.Collections;
using UnityEngine;
using TMPro;

public class Dialogue : MonoBehaviour
{
    public TextMeshProUGUI textComponent;
    public string[] lines;
    [Header ("Text Speed (The smaller, the faster)")]
    public float textSpeed;
    private int index;

    [SerializeField] public GameObject triggerActivity;

    // Start is called before the first frame update
    void Start()
    {
        textComponent.text = string.Empty;
        StartDialogue();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    void StartDialogue()
    {
        index = 0;
        StartCoroutine(TypeLine());
    }
    IEnumerator TypeLine()
    {
        int size = lines[index].ToCharArray().Length;
        int count = 0;
        foreach (char c in lines[index].ToCharArray())
        {
            count++;
            textComponent.text += c;
            yield return new WaitForSeconds(textSpeed);
            if (count == size)
            {
                if (!pauseMenu.GameIsPaused == true)
                {
                    if (textComponent.text == lines[index])
                    {
                        NextLine();
                    }
                    else
                    {
                        StopAllCoroutines();
                        textComponent.text = lines[index];
                    }
                }
            }
        }
    }

    void NextLine()
    {
        if (index < lines.Length - 1)
        {
            index++;
            textComponent.text = string.Empty;
            StartCoroutine(TypeLine());
        }
        else
        {
            gameObject.SetActive(false);
            /*
                We set an empty object deactivated in the scene, we activate it here and
                read it in another script so an action can happen in that script 
            */
            if (triggerActivity != null)
            {
                triggerActivity.SetActive(true);
            }
        }
    }
}
