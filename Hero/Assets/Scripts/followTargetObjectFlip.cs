using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followTargetObjectFlip : MonoBehaviour
{
    public bool playerIsFacingRight = false;
    public float horizontalMovement = 0f;
    void Start()
    {
        playerIsFacingRight = true;
    }

    private void Update()
    {
        horizontalMovement = Input.GetAxisRaw("Horizontal");
    }
    private void FixedUpdate()
    {
        if (horizontalMovement > 0 && !playerIsFacingRight)
        {
            Flip();
        }
        else if (horizontalMovement < 0 && playerIsFacingRight)
        {
            Flip();
        }
    }

    public void Flip()
    {
        playerIsFacingRight = !playerIsFacingRight;
        Vector3 theScale = transform.localPosition;
        theScale.x *= -1;
        transform.localPosition = theScale;
        transform.Rotate(0f, 180f, 0f);
    }
}
