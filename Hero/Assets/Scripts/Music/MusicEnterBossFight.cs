using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using FMOD.Studio;

public class MusicEnterBossFight : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("EnterBossFight", 1);
        
    }

    void Start()
    {
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("EnterBossFight", 0);
    }
}
