using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using FMOD.Studio;

public class MusicEnterAmbience : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("EnterAmbient", 1);
        
    }

    void Start()
    {
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("EnterAmbient", 0);
    }
}
