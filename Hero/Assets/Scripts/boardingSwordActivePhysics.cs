using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TarodevController;
using FMODUnity;

public class boardingSwordActivePhysics : MonoBehaviour
{
    public GameObject Player;
    public GameObject boardingPoint;
    public playerHero PlayerScript;
    public GameObject playerBoardingSprite;
    public followTargetObjectFlip flipScript;
    

    public bool playerIsBoarding;
    private float horizontalMovement = 0f;
    public float MovementPowerWhenBoarding;

    public Vector3 StartimpulseMagnitudeFacingRight = new Vector3(5.0f, 0.0f, 0.0f);
    public Vector3 StartimpulseMagnitudeFacingLeft = new Vector3(5.0f, 0.0f, 0.0f);
    public Vector3 boardingJumpPower = new Vector3(0f, 0f, 0f);
    bool m_oneTime = true;

    [Header("Ground Detection")]
    public groundCheck groundCheckScript;
    public bool swordIsGrounded;

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        boardingPoint = transform.GetChild(2).gameObject;
        playerIsBoarding = false;
        PlayerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<playerHero>();
        playerBoardingSprite.gameObject.GetComponent<SpriteRenderer>().enabled = false;

    }

    // Update is called once per frame
    void Update()
    {
        swordIsGrounded = groundCheckScript.isGrounded;

        if (Input.GetButtonDown("Jump") && swordIsGrounded)
        {
            if (playerIsBoarding)
            {
                SwordJump();
            }
            
        }

        // if (!swordIsGrounded)
        // {
        //     if (playerIsBoarding)
        //     {

        //     }
        // }

        
        horizontalMovement = Input.GetAxisRaw("Horizontal");

       
    }
    private void FixedUpdate()
    {
        //For Adaptive Borading Sound
        var speed = gameObject.GetComponent<Rigidbody2D>().velocity;
        float swordSpeed = speed.magnitude;
        //Debug.Log("Speed of Sword: " + swordSpeed);
        var emitter = GetComponent<FMODUnity.StudioEventEmitter>();
        emitter.SetParameter("swordBoardingSpeed", swordSpeed);

        if (playerIsBoarding)
        {
            transform.position += new Vector3(horizontalMovement, 0, 0) * Time.deltaTime * MovementPowerWhenBoarding;
            Player.transform.position = boardingPoint.transform.position;

        }

        if (m_oneTime)
        {
            if (PlayerScript.playerIsFacingRight)
            {
                flipScript.playerIsFacingRight = true;
                GetComponent<Rigidbody2D>().AddForce(StartimpulseMagnitudeFacingRight, ForceMode2D.Impulse);
                m_oneTime = false;
            } else if (!PlayerScript.playerIsFacingRight)
            {
                flipScript.playerIsFacingRight = false;
                GetComponent<Rigidbody2D>().AddForce(StartimpulseMagnitudeFacingLeft, ForceMode2D.Impulse);
                m_oneTime = false;
            }
        }
    }

    public void SwordJump()
    {
        GetComponent<Rigidbody2D>().AddForce(boardingJumpPower, ForceMode2D.Impulse);
        FMODUnity.RuntimeManager.PlayOneShot("event:/SwordSounds/SwordJumping");
        Debug.Log("Sword Jump!");
    }

    public void Switch()
    {
        if (playerIsBoarding)
        {
            
            playerIsBoarding = false;
            Player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
            

            //Deactivate the Sprite inside the Sword Object and activate Player Sprite 
            PlayerScript.playerSprite.gameObject.GetComponent<SpriteRenderer>().enabled = true;
            playerBoardingSprite.gameObject.GetComponent<SpriteRenderer>().enabled = false;

            //Change Animation State
            PlayerScript.playerAnimator.SetBool("playerIsBoarding", false);

        }
        else if (!playerIsBoarding)
        {

            
            playerIsBoarding = true;

            Player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX |
                                                             RigidbodyConstraints2D.FreezePositionY |
                                                             RigidbodyConstraints2D.FreezeRotation;
            

            //Deactivate Player Sprite and Activate the Sprite inside the Sword Object
            PlayerScript.playerSprite.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            playerBoardingSprite.gameObject.GetComponent<SpriteRenderer>().enabled = true;

            //Change Animation State
            PlayerScript.playerAnimator.SetBool("playerIsBoarding", true);
            FMODUnity.RuntimeManager.PlayOneShot("event:/SwordSounds/SwordDashAttack");

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == Player && !playerIsBoarding)
        {
            Switch();
        }
    }


    
}
