using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class Projectile : MonoBehaviour
{
    public float speed;
    public float deflectSpeed;

    private Transform playerTransform;
    private GameObject player;

    public GameObject explosion;

    //private GameObject bullet;

    public bool deflected = false;
    public Vector2 StartPos;
    public Animator animator;
    
    // void Awake()
    // {
    //     bullet = GameObject.FindGameObjectWithTag("Projectile");
    // }

    
    // Start is called before the first frame update
    void Start()
    {
        StartPos = transform.position;
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        
        //bullet = GameObject.FindGameObjectWithTag("Projectile");

        if (deflected)
        {

            //transform.position = Vector2.MoveTowards(transform.position, player.transform.position, -deflectSpeed * Time.deltaTime);
            transform.position = Vector2.MoveTowards(transform.position, StartPos, deflectSpeed * Time.deltaTime);
            if (transform.position.x == StartPos.x && transform.position.y == StartPos.y)
            {
                DestroyBullet();
            }
        }

        else if (!deflected)
        {
            transform.position = Vector2.MoveTowards(transform.position, playerTransform.position, speed * Time.deltaTime);
        }

        if(transform.position.x == playerTransform.transform.position.x && transform.position.y == playerTransform.transform.position.y)
        {
            DestroyBullet();
        }
        

        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Slash"))
        {
            //Here goes deflecting code
            transform.gameObject.tag = "DeflectedProjectile";
            deflected = true;
            //FMODUnity.RuntimeManager.PlayOneShotAttached("event:/PlayerSounds/PlayerDeflectBullet2", bullet);
            FMODUnity.RuntimeManager.PlayOneShot("event:/SwordSounds/PlayerDeflectBullet2");
            animator.SetBool("deflected", true);
            Debug.Log("Deflected with Slash");
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("boardingSword") || other.gameObject.CompareTag("Enemy"))
        {
            DestroyBullet();
        }
    }

    private void DestroyBullet()
    {
        Destroy(gameObject);
        Instantiate(explosion, transform.position, transform.rotation);
        //play sound
    }

}
