using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using UnityEngine.SceneManagement;
public class playerHero : MonoBehaviour
{
    [Header("Player Stats")]
    [SerializeField] public float attackCooldown;
    [SerializeField] private float coolDownTimer;
    [SerializeField] private float lastTimePlayerAttacked;
    [SerializeField] public bool SwordOfMeaningIsUnlocked;
    [SerializeField] public float AttackDelayTime;

    //Prefabs
    public GameObject Slash1;
    public GameObject Slash2;
    public GameObject boardingSwordActivePrefab;
    public GameObject boardingSwordFollowingPrefab;

    //GameObjects used as Spawn Position for Instantiate
    public GameObject slashPosition;
    public GameObject slashPositionWhenJumping;
    public GameObject spawnSwordActivePositionWhenJumping;
    public GameObject spawnSwordActivePositionWhenGrounded;

    //The Variable of what Sword is being used at the moment
    public GameObject CurrentSword;
    public bool SwordisFollowing;

    //Animation
    public Animator playerAnimator;
    private float horizontalMovement;
    public bool playerIsFacingRight = true;
    public GameObject playerSprite;
    bool enableAttack = true;

    //Physics
    public boardingSwordActivePhysics boardingSwordActivePhysicsScript;
    public bool isGrounded;
    



    void Start()
    {
        if (SwordOfMeaningIsUnlocked)
        {
            //Always Start with the Sword Following the Player
            SwordisFollowing = true;

            Instantiate(boardingSwordFollowingPrefab, transform.position, Quaternion.identity);
        }

            //Connect the slashPosition Object (Child) with the variable slashPosition 
            slashPosition = gameObject.transform.GetChild(2).gameObject;
        slashPositionWhenJumping = gameObject.transform.GetChild(5).gameObject;

            //Connect the spawnSwordActivePosition Object (Child) with the variable spawnSwordActivePositionWhenJumping 
            spawnSwordActivePositionWhenJumping = gameObject.transform.GetChild(3).gameObject;

            //Connect the spawnSwordActivePosition Object (Child) with the variable spawnSwordActivePositionWhenGrounded
            spawnSwordActivePositionWhenGrounded = gameObject.transform.GetChild(4).gameObject;
        

    }

    // Update is called once per frame
    void Update()
    {
        //Animation 
        playerAnimator.SetBool("isGrounded", isGrounded);
        //If the game is paused, we don't check the user input nor we make any change on the user
        if (Time.timeScale != 0f)
        {
            if (SwordOfMeaningIsUnlocked)
            {
                if (Input.GetKeyDown("x") && SwordisFollowing || Input.GetButtonDown("Fire1") && SwordisFollowing)
                {
                   
                    StartCoroutine("SlashAttack");


                }

                else if (Input.GetKeyDown("x") && !SwordisFollowing || Input.GetButtonDown("Fire1") && !SwordisFollowing)
                {
                    //Attack Cooldown Logic
                    if (Time.time - lastTimePlayerAttacked < attackCooldown)
                    {
                        return;
                    }

                    lastTimePlayerAttacked = Time.time;
                    //Connect to the Active Sword Script
                    boardingSwordActivePhysicsScript = CurrentSword.GetComponent<boardingSwordActivePhysics>();

                    if (boardingSwordActivePhysicsScript.playerIsBoarding)
                    {
                        //Tell the boardingSwordActivePhysics Script to Switch
                        boardingSwordActivePhysicsScript.Switch();
                    }


                    SwordisFollowing = true;
                    //Destroy current Sword (each sword assigns itself to this script)
                    Destroy(CurrentSword);
                    //Instantiate Slash in Front of Player
                    //Instantiate(Slash2, slashPosition.transform.position, slashPosition.transform.rotation);
                    //Instantiate Active Boarding Sword
                    Instantiate(boardingSwordFollowingPrefab, CurrentSword.transform.position, transform.rotation);
                    FMODUnity.RuntimeManager.PlayOneShot("event:/SwordSounds/SwordIn");

                }

                
            }
            //Animation
            horizontalMovement = Input.GetAxisRaw("Horizontal");
            playerAnimator.SetFloat("speed", Mathf.Abs(horizontalMovement));
        }
      
    }


    private void FixedUpdate()
    {

        if (horizontalMovement > 0 && !playerIsFacingRight)
        {
            playerIsFacingRight = true;
        }
        else if (horizontalMovement < 0 && playerIsFacingRight)
        {
            playerIsFacingRight = false;
        }
    }

    private IEnumerator SlashAttack()
    {
        //Attack Cooldown Logic
        if (Time.time - lastTimePlayerAttacked < attackCooldown)
        {
            yield break;
        }
        FMODUnity.RuntimeManager.PlayOneShot("event:/PlayerSounds/Jump");
        playerAnimator.SetTrigger("slashAttack");
        SwordisFollowing = false;
        lastTimePlayerAttacked = Time.time;
      
        //Instantiate Active Boarding Sword in the Correct Place
        if (isGrounded)
        {
            yield return new WaitForSeconds(AttackDelayTime);
            //Instantiate Slash in Front of Player
            Instantiate(Slash1, slashPosition.transform.position, slashPosition.transform.rotation);
            //Destroy current Sword (each sword assigns itself to this script)
            Destroy(CurrentSword);
            Instantiate(boardingSwordActivePrefab, spawnSwordActivePositionWhenGrounded.transform.position, spawnSwordActivePositionWhenGrounded.transform.rotation);
        }
        else if (!isGrounded)
        {
            //Destroy current Sword (each sword assigns itself to this script)
            Destroy(CurrentSword);
            Instantiate(Slash2, slashPositionWhenJumping.transform.position, slashPositionWhenJumping.transform.rotation);
            Instantiate(boardingSwordActivePrefab, spawnSwordActivePositionWhenJumping.transform.position, spawnSwordActivePositionWhenJumping.transform.rotation);
        }

        FMODUnity.RuntimeManager.PlayOneShot("event:/SwordSounds/SwordOut");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Projectile") || collision.gameObject.CompareTag("Enemy"))
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/Bullets/BulletDestroy");
            Die();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Explosion2"))
        {
            Die();
        }
    }

    public void Die()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 0);
        FMODUnity.RuntimeManager.PlayOneShot("event:/PlayerSounds/PlayerDeath");
    }
}


